package com.sela.greetingandfun.dao;

import com.sela.greetingandfun.model.Card;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CardRepository  extends MongoRepository<Card, String> {
}
