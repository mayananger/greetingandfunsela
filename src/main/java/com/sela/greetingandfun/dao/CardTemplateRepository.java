package com.sela.greetingandfun.dao;

import com.sela.greetingandfun.model.CardTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CardTemplateRepository extends MongoRepository<CardTemplate, String> {
}
