package com.sela.greetingandfun.service.impl;

import com.sela.greetingandfun.dao.CardTemplateRepository;
import com.sela.greetingandfun.model.*;
import com.sela.greetingandfun.model.request.AddCardTemplateRequest;
import com.sela.greetingandfun.model.response.AddCardTemplateResponse;
import com.sela.greetingandfun.model.response.Response;
import com.sela.greetingandfun.service.intr.AddCardTemplateService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolationException;
import java.util.TreeSet;

public class AddCardTemplateServiceImpl implements AddCardTemplateService {

    @Autowired
    private CardTemplateRepository cardTemplateRepository;

    @Override
    public AddCardTemplateResponse execute(AddCardTemplateRequest request) {
        AddCardTemplateResponse response = new AddCardTemplateResponse(StatusRole.ERROR, new TreeSet<>(), null);

        if (request == null || request.getCardTemplate() == null) {
            response.getErrorRoles().add(ErrorRole.MISSING_VALUE);
            return response;
        }

        CardTemplate cardTemplate = request.getCardTemplate();
        if (cardTemplate.getName() == null || cardTemplate.getSections() == null || cardTemplate.getSections().isEmpty()) {
            response.getErrorRoles().add(ErrorRole.MISSING_VALUE);
            return response;
        }

        if (!checkDuplicateFieldsName(cardTemplate, response)) {
            // check not approve
            return response;
        }

        try {
            cardTemplate.setId(null); // Just in case
            cardTemplate = cardTemplateRepository.save(cardTemplate);
        } catch (ConstraintViolationException e) {
            // we have wrong value in the template
            response.getErrorRoles().add(ErrorRole.WRONG_VALUE);
            return response;
        } catch (Exception e) {
            // unexpected error
            return response;
        }

        response.setCardTemplate(cardTemplate);
        response.setStatusRole(StatusRole.OK);
        return response;
    }

    /**
     * Its not ok to get duplicate names in the templates fields, or there is field without name.
     * this methods will check this
     * @param cardTemplate for run in the fields
     * @param response to set errors codes
     * @return true if every thing is ok
     */
    private static boolean checkDuplicateFieldsName(CardTemplate cardTemplate, Response response) {
        // Run all the field in every section and check for duplicate name or not name at all, in any of those cases the method will return false
        TreeSet<String> fieldNames = new TreeSet<>();
        for (Section section : cardTemplate.getSections()) {
            if (section.getFields() == null) {
                continue;
            }
            // Run all the fields in section
            for (Field field : section.getFields()) {
                if (field.getFieldName() == null) {
                    // Ops its impossible to have field without filed name
                    response.getErrorRoles().add(ErrorRole.MISSING_FIELD_NAME);
                    return false;
                }
                if (fieldNames.contains(field.getFieldName())) {
                    // Ops duplicate name
                    response.getErrorRoles().add(ErrorRole.DUPLICATE_FIELD_NAME);
                    return false;
                }
                fieldNames.add(field.getFieldName());
            }
        }

        return true;
    }


}
