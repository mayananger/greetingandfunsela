package com.sela.greetingandfun.service.impl;

import com.mongodb.MongoException;
import com.sela.greetingandfun.dao.CardTemplateRepository;
import com.sela.greetingandfun.model.StatusRole;
import com.sela.greetingandfun.model.request.GetCardTemplatesRequest;
import com.sela.greetingandfun.model.response.GetCardTemplatesResponse;
import com.sela.greetingandfun.service.intr.GetTemplatesService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.TreeSet;

public class GetTemplatesServiceImpl implements GetTemplatesService {

    @Autowired
    private CardTemplateRepository cardTemplateRepository;

    @Override
    public GetCardTemplatesResponse execute(GetCardTemplatesRequest request) {
        GetCardTemplatesResponse response = new GetCardTemplatesResponse(StatusRole.ERROR, new TreeSet<>(), null);

        try {
            response.setTemplates(cardTemplateRepository.findAll());
        } catch (Exception e) {
            // Unexpected error
            return response;
        }

        response.setStatusRole(StatusRole.OK);
        return response;
    }

}
