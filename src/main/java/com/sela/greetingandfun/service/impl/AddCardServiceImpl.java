package com.sela.greetingandfun.service.impl;

import com.mongodb.MongoException;
import com.sela.greetingandfun.dao.CardRepository;
import com.sela.greetingandfun.dao.CardTemplateRepository;
import com.sela.greetingandfun.model.*;
import com.sela.greetingandfun.model.request.AddCardRequest;
import com.sela.greetingandfun.model.response.AddCardResponse;
import com.sela.greetingandfun.model.response.Response;
import com.sela.greetingandfun.service.intr.AddCardService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolationException;
import java.util.TreeSet;

public class AddCardServiceImpl implements AddCardService {

    @Autowired
    private CardTemplateRepository cardTemplateRepository;
    @Autowired
    private CardRepository cardRepository;

    @Override
    public AddCardResponse execute(AddCardRequest request) {
        AddCardResponse response = new AddCardResponse(StatusRole.ERROR, new TreeSet<>(), null);

        if (request == null || request.getCard() == null) {
            response.getErrorRoles().add(ErrorRole.MISSING_VALUE);
            return response;
        }

        Card card = request.getCard();

        // Get the template from the DB
        CardTemplate cardTemplate = null;
        if (card.getCardTemplate() != null && card.getCardTemplate().getId() != null) {
            cardTemplate = cardTemplateRepository.findOne(card.getCardTemplate().getId());
        }

        if (card.getValues() == null || card.getValues().isEmpty() || card.getCardName() == null) {
            response.getErrorRoles().add(ErrorRole.MISSING_VALUE);
        }

        if (cardTemplate == null) {
            response.getErrorRoles().add(ErrorRole.TEMPLATE_NOT_EXISTS);
        }

        if (!response.getErrorRoles().isEmpty()) {
            // Return errors
            return response;
        }

        if(!allValuesAssignedRight(card, cardTemplate, response)) {
            // method not approve
            return response;
        }

        try {
            card.setId(null); // Just in case
            card = cardRepository.save(card);
        } catch (ConstraintViolationException e) {
            // we have wrong value with one of the fields
            response.getErrorRoles().add(ErrorRole.WRONG_VALUE);
            return response;
        } catch (Exception e) {
            // unexpected error
            return response;
        }
        response.setCard(card);
        response.setStatusRole(StatusRole.OK);
        return response;
    }

    /**
     * will check all field in template, all values in card, to make sure every Field assign as MUST have value, and every field with DISABLE have no value
     * @param card from request
     * @param template from DB
     * @param response for write errors if needed
     * @return true if everything ok
     */
    private static boolean allValuesAssignedRight(Card card, CardTemplate template, Response response) {
        for (Section section : template.getSections()) {
            // run on every field in the template
            for (Field field : section.getFields()) {
                // value by user to this specific field
                String value = card.getValues().get(field.getFieldName());
                if (field.getValueFillRole() == ValueFillRole.MUST && value == null) {
                    // This field assign as MUST field and the user didt give data here. Its unaccepted
                    response.getErrorRoles().add(ErrorRole.FIELD_VALUE_MISSING);
                    return false;
                } else if (field.getValueFillRole() == ValueFillRole.DISABLE && value != null) {
                    // This field assign as DISABLE field and the user give data here. Its not ok and return error
                    response.getErrorRoles().add(ErrorRole.DISABLE_DATA_GIVING);
                    return false;
                }

            }
        }
        return true;
    }

}
