package com.sela.greetingandfun.service.intr;

import com.sela.greetingandfun.model.request.AddCardTemplateRequest;
import com.sela.greetingandfun.model.response.AddCardTemplateResponse;
import com.sela.greetingandfun.service.GreetingService;

public interface AddCardTemplateService extends GreetingService<AddCardTemplateRequest, AddCardTemplateResponse> {
}
