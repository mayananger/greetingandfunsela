package com.sela.greetingandfun.service.intr;

import com.sela.greetingandfun.model.request.GetCardTemplatesRequest;
import com.sela.greetingandfun.model.response.GetCardTemplatesResponse;
import com.sela.greetingandfun.service.GreetingService;

public interface GetTemplatesService extends GreetingService<GetCardTemplatesRequest, GetCardTemplatesResponse> {
}
