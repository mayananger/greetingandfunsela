package com.sela.greetingandfun.service.intr;

import com.sela.greetingandfun.model.request.AddCardRequest;
import com.sela.greetingandfun.model.response.AddCardResponse;
import com.sela.greetingandfun.service.GreetingService;

public interface AddCardService extends GreetingService<AddCardRequest, AddCardResponse> {
}
