package com.sela.greetingandfun.service;

import com.sela.greetingandfun.model.request.Request;
import com.sela.greetingandfun.model.response.Response;

public interface GreetingService<REQ extends Request, RES extends Response> {
    public abstract RES execute(REQ request);
}
