package com.sela.greetingandfun.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Field can represent text or image, field can be must have value while user create card from template. can be optional,
 * or disable (if template creator what to put static view on the card) see also {@link ValueFillRole}
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = FieldImage.class),
        @JsonSubTypes.Type(value = FieldText.class),
})
public abstract class Field {

    @NotNull
    @NotBlank
    @Pattern(regexp = AbstractModel.NAME_PATTERN)
    private String fieldName;

    // Default "LEFT"
    private LayoutRole layoutRole;

    // Default "OPTIONAL"
    private ValueFillRole valueFillRole;

    public Field(String fieldName, LayoutRole layoutRole, ValueFillRole valueFillRole) {
        this.fieldName = fieldName;
        this.layoutRole = layoutRole;
        this.valueFillRole = valueFillRole;
    }

    public LayoutRole getLayoutRole() {
        return layoutRole;
    }

    public void setLayoutRole(LayoutRole layoutRole) {
        this.layoutRole = layoutRole;
    }

    public ValueFillRole getValueFillRole() {
        return valueFillRole;
    }

    public void setValueFillRole(ValueFillRole valueFillRole) {
        this.valueFillRole = valueFillRole;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
