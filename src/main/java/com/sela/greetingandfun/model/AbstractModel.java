package com.sela.greetingandfun.model;

import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Abstract values to each document
 */
public abstract class AbstractModel {

    public static final String NAME_PATTERN = "^[^@.\"']+$";
    public static final String HEX_COLOR_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";

    @Id
    private String id;
    @CreatedDate
    @NotNull
    private LocalDateTime createDate;
    @LastModifiedDate
    @NotNull
    private LocalDateTime changeDate;

    // If we will add Identification to the app, we will able to add "who create field", "who change last time" field
    // with @LastModifiedBy and @CreatedBy nad maybe with OAuth2


    public AbstractModel(String id, LocalDateTime createDate, LocalDateTime changeDate) {
        this.id = id;
        this.createDate = createDate;
        this.changeDate = changeDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(LocalDateTime changeDate) {
        this.changeDate = changeDate;
    }
}
