package com.sela.greetingandfun.model;

// Size for the image
public enum SizeRole {
    SMALL,
    MEDIUM,
    LARGE
}
