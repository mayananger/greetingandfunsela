package com.sela.greetingandfun.model;

public enum ErrorRole {
    WRONG_VALUE, // User puts wrong value
    MISSING_VALUE, // Necessary data missing
    TEMPLATE_NOT_EXISTS, // Template not found or exists
    MISSING_FIELD_NAME, // We got in "create template" action a field without a name
    DUPLICATE_FIELD_NAME, // We got in "create template" duplicate fields name in the same template
    FIELD_VALUE_MISSING, // When user try create a card with MUST data in a field, And user didt fill this data
    DISABLE_DATA_GIVING, // When user try create a card with MUST data in the template
}
