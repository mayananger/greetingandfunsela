package com.sela.greetingandfun.model;

public class FieldText extends Field {

    // Before the value
    private String prefix;

    // After the value
    private String suffix;

    public FieldText(String nameField, LayoutRole layoutRole, ValueFillRole valueFillRole, String prefix, String suffix) {
        super(nameField, layoutRole, valueFillRole);
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public FieldText() {
        this(null, null, null, null, null);
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
