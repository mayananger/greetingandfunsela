package com.sela.greetingandfun.model;

/**
 * where in horizontal way the field will be located
 */
public enum LayoutRole {
    LEFT,
    RIGHT,
    CENTER
}
