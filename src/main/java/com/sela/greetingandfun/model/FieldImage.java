package com.sela.greetingandfun.model;

public class FieldImage extends Field {

    // Default "medium"
    private SizeRole size;
    // If user not give a value to this field, this will be the src
    private String defaultSrc;

    public FieldImage(String fieldName, LayoutRole layoutRole, ValueFillRole valueFillRole, SizeRole size, String defaultSrc) {
        super(fieldName, layoutRole, valueFillRole);
        this.size = size;
        this.defaultSrc = defaultSrc;
    }

    public FieldImage() {
        this(null, null, null, null, null);
    }

    public SizeRole getSize() {
        return size;
    }

    public void setSize(SizeRole size) {
        this.size = size;
    }

    public String getDefaultSrc() {
        return defaultSrc;
    }

    public void setDefaultSrc(String defaultSrc) {
        this.defaultSrc = defaultSrc;
    }
}
