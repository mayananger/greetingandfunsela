package com.sela.greetingandfun.model;

/**
 * For field, set the relationship with the user and the field
 */
public enum ValueFillRole {
    OPTIONAL, // User can puts value of his own while create card
    MUST, // User must puts value of his own while create card
    DISABLE, // User cant puts value of his own while create card. The only value will be prefix / suffix / defaultSrc (static view on the card)
}
