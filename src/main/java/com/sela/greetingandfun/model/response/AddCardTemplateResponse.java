package com.sela.greetingandfun.model.response;

import com.sela.greetingandfun.model.CardTemplate;
import com.sela.greetingandfun.model.ErrorRole;
import com.sela.greetingandfun.model.StatusRole;

import java.util.Set;

public class AddCardTemplateResponse extends Response {

    private CardTemplate cardTemplate;

    public AddCardTemplateResponse(StatusRole statusRole, Set<ErrorRole> errorRoles, CardTemplate cardTemplate) {
        super(statusRole, errorRoles);
        this.cardTemplate = cardTemplate;
    }

    public AddCardTemplateResponse() {
        this(null, null, null);
    }

    public CardTemplate getCardTemplate() {
        return cardTemplate;
    }

    public void setCardTemplate(CardTemplate cardTemplate) {
        this.cardTemplate = cardTemplate;
    }
}
