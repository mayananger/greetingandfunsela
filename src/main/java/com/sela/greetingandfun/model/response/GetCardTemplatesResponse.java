package com.sela.greetingandfun.model.response;

import com.sela.greetingandfun.model.CardTemplate;
import com.sela.greetingandfun.model.ErrorRole;
import com.sela.greetingandfun.model.StatusRole;

import java.util.List;
import java.util.Set;

public class GetCardTemplatesResponse extends Response {

    private List<CardTemplate> templates;

    public GetCardTemplatesResponse(StatusRole statusRole, Set<ErrorRole> errorRoles, List<CardTemplate> templates) {
        super(statusRole, errorRoles);
        this.templates = templates;
    }

    public GetCardTemplatesResponse() {
        this(null, null, null);
    }

    public List<CardTemplate> getTemplates() {
        return templates;
    }

    public void setTemplates(List<CardTemplate> templates) {
        this.templates = templates;
    }
}
