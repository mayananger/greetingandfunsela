package com.sela.greetingandfun.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.sela.greetingandfun.model.ErrorRole;
import com.sela.greetingandfun.model.StatusRole;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = AddCardResponse.class),
        @JsonSubTypes.Type(value = GetCardTemplatesResponse.class),
        @JsonSubTypes.Type(value = AddCardTemplateResponse.class),
})
public abstract class Response {

    private StatusRole statusRole;
    private Set<ErrorRole> errorRoles;

    public Response(StatusRole statusRole, Set<ErrorRole> errorRoles) {
        this.statusRole = statusRole;
        this.errorRoles = errorRoles;
    }

    public StatusRole getStatusRole() {
        return statusRole;
    }

    public void setStatusRole(StatusRole statusRole) {
        this.statusRole = statusRole;
    }

    public Set<ErrorRole> getErrorRoles() {
        return errorRoles;
    }

    public void setErrorRoles(Set<ErrorRole> errorRoles) {
        this.errorRoles = errorRoles;
    }

}
