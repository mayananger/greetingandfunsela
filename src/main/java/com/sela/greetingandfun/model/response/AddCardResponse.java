package com.sela.greetingandfun.model.response;

import com.sela.greetingandfun.model.Card;
import com.sela.greetingandfun.model.ErrorRole;
import com.sela.greetingandfun.model.StatusRole;

import java.util.List;
import java.util.Set;

public class AddCardResponse extends Response {

    private Card card;

    public AddCardResponse(StatusRole statusRole, Set<ErrorRole> errorRoles, Card card) {
        super(statusRole, errorRoles);
        this.card = card;
    }

    public AddCardResponse() {
        this(null, null, null);
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
