package com.sela.greetingandfun.model;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The Card Template Made up with "Sections" {@link Section}. Every section can have background color, This feature made for more customization.
 * Every Section have fields. A part with value. See more in {@link Field}
 */
@Document
public class CardTemplate extends AbstractModel {

    @NotNull
    @NotBlank
    private String name;
    private String description;
    @NotNull
    private List<Section> sections;

    public CardTemplate(String id, LocalDateTime createDate, LocalDateTime changeDate, String name, String description, List<Section> sections) {
        super(id, createDate, changeDate);
        this.name = name;
        this.description = description;
        this.sections = sections;
    }

    public CardTemplate() {
        this(null, null, null, null, null, null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
