package com.sela.greetingandfun.model;

import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * Section have a fields and color. The section will have padding and margin on display as well.
 * Section is use for customisation
 */
public class Section {

    // Hex color, Default will be white
    @Pattern(regexp = AbstractModel.HEX_COLOR_PATTERN)
    private String color;

    private List<Field> fields;

    public Section(String color, List<Field> fields) {
        this.color = color;
        this.fields = fields;
    }

    public Section() {
        this(null, null);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
