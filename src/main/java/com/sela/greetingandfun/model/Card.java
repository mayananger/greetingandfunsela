package com.sela.greetingandfun.model;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Card in in fact a {@link CardTemplate} with values, the values field represent the values to fill in the necessary places by the fieldName
 */
@Document
public class Card extends AbstractModel {

    @DBRef
    @NotNull
    private CardTemplate cardTemplate;

    /**
     * Key = fieldName (prop of {@link Field}), Value = The field value
     * Right know the value for {@link FieldText} is string, and for {@link FieldImage} is also string (src value),
     * its recommended to replace to more complex way to store data for each field, But with the current complex of the app. That unnecessary
     */
    @NotNull
    private Map<String, String> values;

    @NotNull
    @NotBlank
    private String cardName;

    public Card(String id, LocalDateTime createDate, LocalDateTime changeDate, CardTemplate cardTemplate, Map<String, String> values, String cardName) {
        super(id, createDate, changeDate);
        this.cardTemplate = cardTemplate;
        this.values = values;
        this.cardName = cardName;
    }

    public Card() {
        this(null, null, null, null, null, null);
    }

    public CardTemplate getCardTemplate() {
        return cardTemplate;
    }

    public void setCardTemplate(CardTemplate cardTemplate) {
        this.cardTemplate = cardTemplate;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }
}
