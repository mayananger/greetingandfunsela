package com.sela.greetingandfun.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = AddCardRequest.class),
        @JsonSubTypes.Type(value = GetCardTemplatesRequest.class),
        @JsonSubTypes.Type(value = AddCardTemplateRequest.class),
})
public abstract class Request {

    public static final String
            ADD_CARD_TEMPLATE_PATH = "addcardtemplate",
            ADD_CARD_PATH = "addcard",
            GET_TEMPLATES_PATH = "gettemplates";

}
