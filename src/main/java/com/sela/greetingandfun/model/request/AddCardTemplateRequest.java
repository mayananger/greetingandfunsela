package com.sela.greetingandfun.model.request;

import com.sela.greetingandfun.model.CardTemplate;

public class AddCardTemplateRequest extends Request {

    private CardTemplate cardTemplate;

    public AddCardTemplateRequest(CardTemplate cardTemplate) {
        this.cardTemplate = cardTemplate;
    }

    public AddCardTemplateRequest() {
        this(null);
    }

    public CardTemplate getCardTemplate() {
        return cardTemplate;
    }

    public void setCardTemplate(CardTemplate cardTemplate) {
        this.cardTemplate = cardTemplate;
    }
}
