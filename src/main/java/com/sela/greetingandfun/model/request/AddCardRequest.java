package com.sela.greetingandfun.model.request;

import com.sela.greetingandfun.model.Card;

public class AddCardRequest extends Request {

    private Card card;

    public AddCardRequest(Card card) {
        this.card = card;
    }

    public AddCardRequest() {
        this(null);
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
