package com.sela.greetingandfun.app;

import com.sela.greetingandfun.model.request.AddCardRequest;
import com.sela.greetingandfun.model.request.AddCardTemplateRequest;
import com.sela.greetingandfun.model.request.GetCardTemplatesRequest;
import com.sela.greetingandfun.model.request.Request;
import com.sela.greetingandfun.model.response.AddCardResponse;
import com.sela.greetingandfun.model.response.AddCardTemplateResponse;
import com.sela.greetingandfun.model.response.GetCardTemplatesResponse;
import com.sela.greetingandfun.service.intr.AddCardService;
import com.sela.greetingandfun.service.intr.AddCardTemplateService;
import com.sela.greetingandfun.service.intr.GetTemplatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * App Controller
 */
@RestController
public class GreetingController {

    @Autowired
    private AddCardService addCardService;
    @Autowired
    private AddCardTemplateService addCardTemplateService;
    @Autowired
    private GetTemplatesService getTemplatesService;

    @RequestMapping(method = RequestMethod.POST, value = Request.ADD_CARD_PATH)
    public AddCardResponse addCardResponse(@RequestBody AddCardRequest request) {
        return addCardService.execute(request);
    }

    @RequestMapping(method = RequestMethod.POST, value = Request.ADD_CARD_TEMPLATE_PATH)
    public AddCardTemplateResponse addTemplateResponse(@RequestBody AddCardTemplateRequest request) {
        return addCardTemplateService.execute(request);
    }

    @RequestMapping(value = Request.GET_TEMPLATES_PATH)
    public GetCardTemplatesResponse getTemplatesResponse(@RequestBody GetCardTemplatesRequest request) {
        return getTemplatesService.execute(request);
    }

}
