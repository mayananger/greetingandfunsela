package com.sela.greetingandfun.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.convert.Jsr310Converters;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Welcome to Greeting & Fun, The best greeting card app ! use with fun
 */
@EntityScan(
        basePackageClasses = {
                Application.class,
                Jsr310Converters.class
        }
)
@EnableMongoRepositories(value = "com.sela.greetingandfun")
@SpringBootApplication(scanBasePackages = "com.sela.greetingandfun")
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
