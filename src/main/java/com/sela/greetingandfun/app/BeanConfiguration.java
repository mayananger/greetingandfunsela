package com.sela.greetingandfun.app;

import com.sela.greetingandfun.service.intr.AddCardService;
import com.sela.greetingandfun.service.intr.AddCardTemplateService;
import com.sela.greetingandfun.service.intr.GetTemplatesService;
import com.sela.greetingandfun.service.impl.AddCardServiceImpl;
import com.sela.greetingandfun.service.impl.AddCardTemplateServiceImpl;
import com.sela.greetingandfun.service.impl.GetTemplatesServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Here Bean Configuration
 */
@Configuration
public class BeanConfiguration {

    // Services

    @Bean
    public AddCardService getAddCardService() {
        return new AddCardServiceImpl();
    }

    @Bean
    public AddCardTemplateService getAddCardTemplateService() {
        return new AddCardTemplateServiceImpl();
    }

    @Bean
    public GetTemplatesService getTemplatesService() {
        return new GetTemplatesServiceImpl();
    }

    // Spring resources location

    @Bean
    WebMvcConfigurer configurer () {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addResourceHandlers (ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/res/**").
                        addResourceLocations("classpath:/static/");
            }
        };
    }

}
